-- # If/Then/Else and Guards
mySignum :: (Ord a, Num a) => a -> Integer

{--
-- With If/Then/Else
mySignum x = 
    if x < 0
        then -1
        else if x > 0
            then 1
            else 0
--}

-- With Guards
mySignum x
    | x < 0 = -1
    | x > 0 = 1
    | otherwise = 0

absolute :: (Ord a, Num a) => a -> a

-- With if/then/else
-- absolute x = if x < 0 then -x else x
-- With Guards
absolute x
    | x < 0 = -x
    | x >= 0 = x

-- # Pattern Matching
pts :: Int -> Int

{-- 
-- With if/then/else (ugly!)
pts x =
    if x == 1
        then 10
        else if x == 2
            then 6
            else if x == 3
                then 4
                else if x == 4
                    then 3
                    else if x == 5
                        then 2
                        else if x == 6
                            then 1
                            else 0
--}

-- With Pattern Matching
pts 1 = 10
pts 2 = 6
pts x
    | x <= 0 = 0 -- Exercise fix, didn't account for x < 0 case
    | x <= 6 = 7 - x
    | otherwise = 0

-- # Let Bindings

roots :: Floating a => a -> a -> a -> (a, a)
roots a b c =
    let sdisc = sqrt(b * b - 4 * a * c)
        twice_a = 2 * a
    in ((-b + sdisc) / (twice_a), (-b - sdisc) / (twice_a))