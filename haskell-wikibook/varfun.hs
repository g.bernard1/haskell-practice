r = 5.0
area = pi * r^2

-- Following will fail because of multiple declarations
-- r = 2

y = x * 2
x = 1

double x = 2 * x
quadruple x = double (double x)
square x = x * x
half x = x / 2

-- Where clause

heron a b c = sqrt (s * (s - a) * (s - b) * (s - c))
    where
    s = (a + b + c) / 2