-- ### Simple Input and Output ###

hello = do
    putStrLn "Please enter your name:"
    name <- getLine
    putStrLn ("Hello, " ++ name ++ ", how are ya?")

rectangle = do
    putStrLn "The base?"
    w :: Double <- fmap read getLine
    putStrLn "The height?"
    h :: Double <- fmap read getLine
    let area = show (w * h)
    putStrLn ("The area of that triangle is " ++ area)

doGuessing num = do
   putStrLn "Enter your guess:"
   guess <- getLine
   if (read guess) < num
     then do putStrLn "Too low!"
             doGuessing num
     else if (read guess) > num
            then do putStrLn "Too high!"
                    doGuessing num
            else putStrLn "You Win!"

{-- 
Write a program that asks the user for his or her name. If the name is one of
Simon, John or Phil, tell the user that you think Haskell is a great programming
language. If the name is Koen, tell them that you think debugging Haskell is fun
(Koen Classen is one of the people who works on Haskell debugging); otherwise,
tell the user that you don't know who he or she is.

(As far as syntax goes there are a few different ways to do it; write at least a
version using if / then / else.)
--}

vip = do
    putStrLn "Who are you?"
    name <- getLine
    if (elem name ["Simon", "John", "Phil"]) 
        then do putStrLn "Haskell is a great programming language!"
    else if name == "Koen" 
        then do putStrLn "Debugging Haskell is fun!"
    else
        putStrLn "I don't know you"

{--
Exercises:
# 1. Why does the unsweet version of the let binding require an extra do keyword? 

# 2. Do you always need the extra do?

# 3. (extra credit) Curiously, let without in is exactly how we wrote things when we
    were playing with the interpreter in the beginning of this book. Why is it ok to
    omit the in keyword in the interpreter but needed (outside of do blocks) in a
    source file?
--}

