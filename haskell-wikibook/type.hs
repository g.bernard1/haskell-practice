
-- Exercises

{--
1. Negate is type Int -> Int

Actually, it is type Num a => a -> a

2. What type is (||)

(||) :: Bool -> Bool -> Bool

3. monthLength?

monthLength :: Num a => Bool -> a -> a

--}

-- 4. f :: Bool -> Bool -> Bool
f x y = not x && y

-- 5. g :: Num a => a -> a
g x = (2 * x - 1)^2

xor :: Bool -> Bool -> Bool
xor p q = (p || q) && not (p && q)
