-- Exercises

{-- 1. 
ghci> 3:[True, False]
<interactive>:42:1: error: [GHC-39999]
    • No instance for ‘Num Bool’ arising from the literal ‘3’
    • In the first argument of ‘(:)’, namely ‘3’
      In the expression: 3 : [True, False]
      In an equation for ‘it’: it = 3 : [True, False]

Fails because of trying to cons a different type onto a boolean list
--}

-- 2. Write a function cons8 that takes a list as an argument and conses 8 (at the beginning) on to it.

cons8 xs = 8:xs

{--
ghci> :t cons8
cons8 :: Num a => [a] -> [a]
ghci> cons8 []
[8]
ghci> cons8 [1,2,3]
[8,1,2,3]
ghci> cons8 [True,False]
<interactive>:9:1: error: [GHC-39999]
    • No instance for ‘Num Bool’ arising from a use of ‘cons8’
    • In the expression: cons8 [True, False]
      In an equation for ‘it’: it = cons8 [True, False]
ghci> let foo = cons8 [1,2,3]
ghci> cons8 foo
[8,8,1,2,3]
--}

-- 3.

append8 :: Num a => [a] -> [a]
append8 xs = xs ++ [8]


{--
# 1. 
a) [1, 2, 3, []] INVALID
b) [1, [2, 3], 4] INVALID
c) [[1, 2, 3], []] VALID

# 2. 
a) []:[[1,2,3],[4,5,6]] VALID [[],[1,2,3],[4,5,6]]
b) []:[] VALID [[]]
c) []:[]:[] VALID [[],[]]
d) [1]:[]:[] VALID [[1],[]]
e) ["hi"]:[1]:[] INVALID

# 3.
Can haskell have lists of lists of lists? Yeah ofc...

# 4.
Why is the following invalid?

ghci> [[1,2],3,[4,5]]

This is trying to concatenate a "list of Ints" and an "Int", which are not the same type
--}
